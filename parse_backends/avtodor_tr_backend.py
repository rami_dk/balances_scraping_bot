from lxml import html

from parse_backends import BaseBackend


class AvtodorTRBackend(BaseBackend):
    """Backend for scraping balance from AvtodorTR"""
    def _get_account_url(self):
        return self.BASE_URL + '/account/login'

    def _parse_current_balance(self, account_page_html):
        tree = html.fromstring(account_page_html)
        raw_current_balance = tree.xpath('//div[@id="balans"]/span/text()')[0]
        return raw_current_balance

    def _get_account_page_html(self):
        files = {
            'email': (None, self.login),
            'password': (None, self.password),
            'submit0': (None, ''),
            'return_url': (None, 'https://avtodor-tr.ru/account'),
        }

        response_html = self._request(url=self._get_account_url(), type_='post', files=files)
        return response_html
