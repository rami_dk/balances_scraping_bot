from urllib.parse import urlencode
from lxml import html

from parse_backends import BaseBackend


class RNCardBackend(BaseBackend):
    """Backend for scraping balance from RN Card"""
    def _get_account_url(self):
        return self.BASE_URL + '/Users/ReLogin.aspx'

    def _parse_current_balance(self, account_page_html):
        tree = html.fromstring(account_page_html)
        raw_current_balance = tree.xpath('(//table[contains(@class, "saldo-online-table")]//td)[1]/text()')[0]
        return raw_current_balance

    def _get_account_page_html(self):
        response_html = self._request(self._get_account_url(), 'get')
        tree = html.fromstring(response_html)

        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        data = {
            '__VIEWSTATE': tree.xpath('//input[@name="__VIEWSTATE"]/@value')[0],
            '__VIEWSTATEGENERATOR': tree.xpath('//input[@name="__VIEWSTATEGENERATOR"]/@value')[0],
            '__EVENTVALIDATION': tree.xpath('//input[@name="__EVENTVALIDATION"]/@value')[0],
            'ctl00$contentPlaceHolder$loginBox$txtLogin': self.login,
            'ctl00$contentPlaceHolder$loginBox$txtPassword': self.password,
            'ctl00$contentPlaceHolder$loginBox$btnLogin':
                tree.xpath('//input[@name="ctl00$contentPlaceHolder$loginBox$btnLogin"]/@value')[0]
        }

        response_html = self._request(self._get_account_url(), 'post', data=urlencode(data), headers=headers)

        return response_html
