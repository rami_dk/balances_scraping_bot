import abc
from datetime import datetime

import requests

from parse_backends import backend_settings


class CurrentBalance(object):
    """Balance scraped from service"""
    def __init__(self, settings, amount=None, currency=None, error_msg=None):
        super().__init__()
        if amount is None and error_msg is None:
            raise NotImplementedError('Нужно указать либо amount либо error_msg')

        self.amount = self._clean(amount) if amount is not None else 0
        self.currency = currency if currency else settings['DEFAULT_CURRENCY']
        self.service_name = settings['NAME']
        self.service_code = settings['CODE']
        self.timestamp = datetime.now()
        self.error_msg = error_msg

    def _clean(self, raw_amount):
        """Canonicalizes passed amount. Ex: '120 USD' => 120.00"""
        return round(float(''.join([x for x in raw_amount if x.isdigit() or x in (',', '.')]).replace(',', '.')), 2)

    def __str__(self):
        amount = '{:,.2f}'.format(self.amount).replace(',', ' ') if not self.error_msg else self.error_msg
        return '{service}: {amount}'. \
            format(service=self.service_name, amount=amount)


class BaseBackend(metaclass=abc.ABCMeta):
    """Abstract base class for all backends that are responsible for scraping data from concrete services"""
    def __init__(self):
        super().__init__()
        self.SETTINGS = getattr(backend_settings,
                                '{}_SETTINGS'.format(self.__class__.__name__.upper().replace('BACKEND', '')))
        self.BASE_URL = self.SETTINGS['BASE_URL']
        self.login = self.SETTINGS['LOGIN']
        self.password = self.SETTINGS['PASSWORD']

    def _request(self, url, type_, data=None, headers=None, files=None):
        """Performs request and returns html"""
        session = requests.Session()

        if headers:
            session.headers.update(headers)

        response = getattr(session, type_)(url, data=data, files=files)
        response.raise_for_status()

        return response.text

    def get_current_balance(self) -> CurrentBalance:
        """Main method for getting balance from service"""
        account_page_html = self._get_account_page_html()
        raw_current_amount = self._parse_current_balance(account_page_html)
        return CurrentBalance(settings=self.SETTINGS, amount=raw_current_amount)

    @abc.abstractmethod
    def _parse_current_balance(self, *args, **kwargs) -> str:
        """Finds desired balance value in passed html and returns it.
        Returns raw balance. Ex: '120 USD' """
        pass

    @abc.abstractmethod
    def _get_account_url(self, *args, **kwargs):
        """Full account url where the desired balance is present"""
        pass

    @abc.abstractmethod
    def _get_account_page_html(self, *args, **kwargs):
        """Performs authentication in service account using credentials provided in backend setting.
        Returns html where the desired balance is present"""
        pass


def all_backends():
    """Returns list of all service backends"""
    from parse_backends.avtodor_tr_backend import AvtodorTRBackend
    from parse_backends.elama_backend import ElamaBackend
    from parse_backends.rncard_backend import RNCardBackend
    from parse_backends.transponder_ossp_backend import TransponderOSSPBackend
    from parse_backends.zadarma_backend import ZadarmaBackend
    backends = [RNCardBackend, AvtodorTRBackend, ZadarmaBackend, ElamaBackend, TransponderOSSPBackend]
    return backends
