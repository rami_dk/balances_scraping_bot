from urllib.parse import urlencode
from lxml import html

from parse_backends import BaseBackend


class TransponderOSSPBackend(BaseBackend):
    """Backend for scraping balance from Transponder OSSP"""
    def _get_account_url(self):
        return self.BASE_URL + '/onyma/'

    def _parse_current_balance(self, account_page_html):
        tree = html.fromstring(account_page_html)
        raw_current_balance = tree.xpath('//ul[@class="lkmenu"]//td[contains(@class, "balance-value")]/text()')[0]
        return raw_current_balance

    def _get_account_page_html(self):
        data = {
            'login': self.login,
            'password': self.password,
            'submit': 'Вход'
        }

        headers = {'Content-Type': 'application/x-www-form-urlencoded'}

        response_html = self._request(self._get_account_url(), 'post', data=urlencode(data), headers=headers)

        return response_html
