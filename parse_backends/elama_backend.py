from urllib.parse import urlencode
from lxml import html

from parse_backends import BaseBackend


class ElamaBackend(BaseBackend):
    """Backend for scraping balance from Elama"""
    def _get_account_url(self):
        return self.BASE_URL + '/login_check'

    def _parse_current_balance(self, account_page_html):
        tree = html.fromstring(account_page_html)
        raw_current_balance = tree.xpath('//div[@class="ui_header_balance"]/span/span/text()')[0]
        return raw_current_balance

    def _get_account_page_html(self):
        data = {
            '_username': self.login,
            '_password': self.password,
            'remember_me': 0
        }

        headers = {'Content-Type': 'application/x-www-form-urlencoded'}

        response_html = self._request(self._get_account_url(), 'post', data=urlencode(data), headers=headers)

        return response_html
