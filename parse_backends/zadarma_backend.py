from urllib.parse import urlencode
from lxml import html

from parse_backends import BaseBackend


class ZadarmaBackend(BaseBackend):
    """Backend for scraping balance from Zadarma"""
    def _get_account_url(self):
        return self.BASE_URL + '/auth/'

    def _parse_current_balance(self, account_page_html):
        tree = html.fromstring(account_page_html)
        raw_current_balance = tree.xpath('//div[@id="navbar"]//a[@class="balance"]/text()')[0]
        return raw_current_balance

    def _get_account_page_html(self):
        response_html = self._request(self._get_account_url(), 'get')
        tree = html.fromstring(response_html)
        inputs = tree.xpath('//form[@id="login-form"]//input')

        data = {x.name: x.value for x in inputs}
        data['email'] = self.login
        data['password'] = self.password

        headers = {'Content-Type': 'application/x-www-form-urlencoded'}

        response_html = self._request(self._get_account_url(), 'post', data=urlencode(data), headers=headers)

        return response_html
