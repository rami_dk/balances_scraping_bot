import logging


class RequireLoggingLevelDebug(logging.Filter):
    def filter(self, record):
        return record.levelname == 'DEBUG'
