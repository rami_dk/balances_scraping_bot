import logging
import smtplib
from email.message import EmailMessage

import settings


class EmailBackend(object):
    """Backend for sending response by email"""
    def _get_smtp_session(self):
        """Returns authenticated SMTP session object"""
        port = getattr(settings, 'EMAIL_PORT', 0)
        if getattr(settings, 'EMAIL_USE_SSL', False):
            session = smtplib.SMTP_SSL(host=settings.EMAIL_HOST, port=port)
        else:
            session = smtplib.SMTP(host=settings.EMAIL_HOST, port=port)

        session.login(user=settings.EMAIL_HOST_USER, password=settings.EMAIL_HOST_PASSWORD)

        return session

    def send_email(self, subject, text, from_=None, to_: (list, tuple) = ()):
        """Sends an email"""
        msg = EmailMessage()
        msg.set_content(text)
        msg['Subject'] = subject
        msg['From'] = from_ or settings.DEFAULT_FROM_EMAIL
        msg['To'] = ', '.join(to_ or settings.DEFAULT_TO_EMAIL)

        session = self._get_smtp_session()

        log_str = 'Subject: {subject}, From: {from_}, To: {to}'.format(subject=msg['Subject'], from_=msg['From'],
                                                                       to=msg['To'])
        try:
            session.send_message(msg)
        except Exception:
            logging.exception('Error sending email: %s' % log_str)
        else:
            logging.info('Email successfully sent: %s' % log_str)
        finally:
            session.quit()

    def send_data(self, response):
        text = response.text()
        self.send_email(subject=response.subject, text=text, to_=response.to_)
