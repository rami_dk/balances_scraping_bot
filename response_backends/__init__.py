import abc
import inspect
import logging
import traceback

import datetime

import settings
from parse_backends import all_backends, CurrentBalance
from response_backends.email_backend import EmailBackend


class BaseResponse(metaclass=abc.ABCMeta):
    """Abstract base class for response"""
    def __init__(self, *args, **kwargs):
        super().__init__()

    @abc.abstractmethod
    def get_response_data(self, *args, **kwargs):
        """Collects response data"""
        pass

    @abc.abstractmethod
    def text(self) -> str:
        """Formats response data"""
        pass

    @property
    @abc.abstractmethod
    def subject(self) -> str:
        """Returns human readable subject for response"""
        pass


class BalanceResponse(BaseResponse):
    """Response containing successfully scraped balances.
    Sends to settings.DEFAULT_TO_EMAIL by default."""
    def __init__(self, to_: (list, tuple) = ()):
        self.response_data = self.get_response_data()
        self.to_ = to_ or settings.DEFAULT_TO_EMAIL
        super().__init__()

    def get_response_data(self):
        """Collects balances from all services that have been successfully scraped and emails them.
        In case of error additionally emails ErrorResponse."""
        backends = all_backends()
        balances = []
        for backend in backends:
            backend_instance = backend()
            try:
                current_balance = backend_instance.get_current_balance()
                logging.info('Balance received: %s' % current_balance)
            except Exception as e:
                logging.exception('Error getting balance on %s' % backend)
                EmailBackend().send_data(ErrorResponse(e, backend=backend))
                balances.append(CurrentBalance(settings=backend_instance.SETTINGS, error_msg='Error scraping balances.'))
            else:
                balances.append(current_balance)
        return balances

    def text(self):
        now = datetime.datetime.now()

        text = 'Good moorning!\nBelow are the balances of personal accounts of {}:\n'.format(now.date().strftime('%d.%m.%Y'))
        text += '\n'.join([str(x) for x in self.response_data])
        text += '\n\n--\nAssistant-Bot.'
        return text

    @property
    def subject(self):
        now = datetime.datetime.now()
        return 'Balances of %s' % now.date().strftime('%d.%m.%Y')


class ErrorResponse(BaseResponse):
    """Response containing traceback, error, variables and (if provided) backend.
    Sends to settings.DEFAULT_ERROR_TO_EMAIL by default."""
    def __init__(self, error, backend=None, to_: (list, tuple) = (), *args, **kwargs):
        self.get_response_data(error, backend)
        self.to_ = to_ or settings.DEFAULT_ERROR_TO_EMAIL
        super().__init__()

    def get_response_data(self, error, backend):
        """Collects error, backend, traceback and variables"""
        self.error = error
        self.backend = backend
        self.traceback = traceback.format_exc()
        self.variables = inspect.trace()[-1][0].f_locals

    def text(self):
        variables = '\n'.join(['{}: {}'.format(key, repr(value)) for key, value in self.variables.items()])
        context = [repr(self.error), self.traceback, variables]

        if self.backend:
            context = [self.backend.__name__, ] + context

        text = '\n\n'.join(context)
        return text

    @property
    def subject(self):
        return 'Balance scraping failed.'
