import argparse
from logging import config
import logging

from settings import LOGGING


if __name__ == "__main__":
    config.dictConfig(LOGGING)

    parser = argparse.ArgumentParser(description='Bot commands list')

    parser.add_argument("command", type=str,
                        help="command to run",)

    args, unknown_args = parser.parse_known_args()
    logging.info('Requested command: %s' % args.command)

    try:
        __import__('commands.' + args.command)
        getattr(__import__('commands.' + args.command), args.command).run()
    except:
        logging.exception('Error running command')
