import logging

from response_backends import BalanceResponse
from response_backends.email_backend import EmailBackend


def run():
    """Sends email with latest balance data"""
    logging.info('Command started')
    EmailBackend().send_data(BalanceResponse())
    logging.info('Command successfully finished')
