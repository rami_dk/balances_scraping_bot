## Bot for collecting remaining balances in services and sending email report

#### Collects data from the following services:
* RN Card: https://lk.rn-card.ru
* Avtodor TR: https://avtodor-tr.ru
* Zadarma: https://my.zadarma.com
* Elama: https://account.elama.ru

#### Available commands:
* Collects remaining balances from services and sends email: `python main.py send_email`