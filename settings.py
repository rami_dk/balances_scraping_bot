try:
    from local_settings import *  # noqa
    import local_settings
except:
    pass

LOG_FILES_DIR = getattr(local_settings, 'LOG_FILES_DIR', os.path.join(os.path.dirname(__file__), 'logs'))

if not os.path.exists(LOG_FILES_DIR):
    os.makedirs(LOG_FILES_DIR)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'detail': {
            'format': '%(asctime)s %(thread)d %(levelname)s %(name)s [%(module)s %(filename)s %(funcName)s %(lineno)d]: %(message)s'
        }
    },
    'filters': {
        'require_logging_level_debug': {
                '()': 'logging_.RequireLoggingLevelDebug'
            }
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'detail',
            'filters': ['require_logging_level_debug', ]
        },
        'main_log': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(LOG_FILES_DIR, 'main_log.txt'),
            'maxBytes': 1024 * 1024 * 5,  # 5 Mb
            'formatter': 'detail'
        },
        'error_log': {
            'level': 'ERROR',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(LOG_FILES_DIR, 'error_log.txt'),
            'maxBytes': 1024 * 1024 * 5,  # 5 Mb
            'formatter': 'detail'
        },
    },
    'loggers': {
        '': {
            'handlers': ['console', 'main_log', 'error_log'],
            'level': getattr(local_settings, 'LOGGING_LEVEL', 'INFO'),
            'propagate': True
        },
    }
}
